# Research Repository #

CSCI 5710 | Spring 2018 | Research for the department website

## Structure ##

There are two root directories, one for each of the broad research areas (content vs. methods), mirroring the sheets in the Excel workbook.

The subdirectories should reflect the outline structure of worksheets to the leaves or leaves - 1. Leaf topics do not have their own directories, on the assumption that many leaf topics will only require one document or a set of documents contained in the directory for their parent node. Feel free to create additional subdirectories to organize your work, but the topmost 3-4 directory levels should continue to match the Excel worksheet outlines, otherwise we'll all go crazy.

Because git tends not to like empty directories, I have put text files in each, either `subtopics.txt` (listing the relevant leaf nodes for that directory) or `no-subtopics.txt` (I didn't do a perfect job and this directory ended up empty). As you add actual content, feel free to delete these files. They're only really there to make sure the directory structure is committed.

## Assignments and canonicity and stuff ##

I have not indicated assignments to topics anywhere in the repository other than by including the Excel workbook from Phil in the root directory. In any case where the directory structure of the repository differs from the Excel sheets, assume the Excel file is correct and the repository is wrong; feel free to open an issue or, for small differences, correct and commit the changes yourself.

As new versions of the Excel workbook are distributed, old versions will be retired into a folder, and the latest version should always be in the root directory.

## Contribution workflow ##

The organization system and kinds of files you commit to your assigned subdirectories are (at least initially) entirely up to you; use whatever is most appropriate for your topics.  For text or rich text content Word documents, RTF, or Markdown are all good options.

Because many people will be working simultaneously, I _strongly recommend_ creating git branches for your work rather than working on the master branch. This will allow you to rebase your work on the latest version of the repo as other people begin to add contributions. (If merging becomes an issue, I may resort to locking the master branch and enforcing the use of pull requests. That sounds annoying. Let's not do that.)

## And lastly ##

This repository is currently _public._ BitBucket asks for money if we add more than 5 people to a private repository. If there is research content we need to keep confidential, notify Dr. Pfeiffer and we'll figure it out.
