----------------------------------------------------------
 	Taking Graduate-Level Courses as an Undergraduate
----------------------------------------------------------

I was unable to find any information on the departmental site
about taking graduate courses as an undergraduate. I spoke with
Dr. Barrett and gathered the following information from the 
Graduate Catalog: 

	"A senior, lacking no more than nine semester hours for 
	graduation at East Tennessee State University, may register for 
	graduate coursework. The total course load for a senior enrolled 
	for graduate coursework may not exceed 12 credit hours. 
	Requirements for the undergraduate degree must be completed during 
	the semester in which the student is allowed to register for 
	part-time graduate work. Petition forms, available in the School 
	of Graduate Studies, must be signed by the undergraduate department 
	chair and the dean of the School of Graduate Studies."