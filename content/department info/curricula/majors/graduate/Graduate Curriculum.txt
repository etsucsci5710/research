Curriculum � Graduate � Concentrations � Applied Computer Science (ACS) and Information Technology (IT)

Observation
i.	No brief description of the individual courses listed in the department�s website
ii.	No link connecting the catalogue page to the department�s program of study page
iii.	The concentration�s overview does not state the major difference in both concentration
iv.	The information on the website is not accurate. (e.g. on the department�s website, software verification and validation is listed as an elective for students in ACS. this is not true) 
v.	The information presentation is not uniform. (the font color used for courses listed under ACS is black while courses listed under IT is gray)
vi.	The course schedule is not clearly stated � the different times the courses would be taught in a semester is not clearly stated
vii.	The information presented on the department�s website and the catalogue have discrepancies.

Review
i.	Each concentration should have its own page
ii.	A brief description of the concentration should be clearly stated
iii.	The calendar of the course should be clearly stated. (courses taken alternatively should be clearly indicated).
iv.	The information on the department�s website should be tally with the information on the catalogue
v.	There should be a comprehensive faculty directory that states contact information, their research areas and courses they have taught or are currently teaching.
vi.	The following links should be added in the body of the text:
	a.	link to the catalogue which gives a brief description of each course to the department�s website - http://catalog.etsu.edu/preview_program.php?catoid=20&poid=7617&returnto=1018
	b.	link to the admission page which gives instruction on how to apply to the department - https://www.etsu.edu/gradstud/applynow.php
	c.	link to the graduate school. This link takes a user to the graduate�s school page - https://www.etsu.edu/gradstud/
	d.	link to the faculty directory (provided each faculty member have an overview description of themselves). This would give a user an opportunity to establish a personal communication with a faculty member-  https://www.etsu.edu/cbat/computing/facstaff/
	e.	link to the library. This link gives the user an opportunity to surf the books available in the library as it relates to the course description -  https://libraries.etsu.edu/home


Links
i.	http://catalog.etsu.edu/preview_program.php?catoid=20&poid=7617&returnto=1018
ii.	https://www.etsu.edu/cbat/computing/graduate_students/programs_of_study.php
iii.	http://catalog.etsu.edu/content.php?catoid=20&catoid=20&navoid=1012&filter%5Bitem_type%5D=3&filter%5Bonly_active%5D=1&filter%5B3%5D=1&filter%5Bcpage%5D=4#acalog_template_course_filter
iv.	https://www.etsu.edu/cbat/computing/facstaff/
v.	https://libraries.etsu.edu/home
