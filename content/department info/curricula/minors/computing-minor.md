# Computing Minor #

The department offers a Minor in computing. Currently, information on the minor cannot be found on the department website; neither is it mentioned on the department page in the university catalog 
(Catalog > Colleges/Schools/Departments > CBAT > Computing) 
http://catalog.etsu.edu/preview_entity.php?catoid=21&ent_oid=674&returnto=1064

Information on the minor is available in the university catalog via Catalog > 'Programs by Type (Majors, Minors, Pre-Professional Studies, etc.)' > Minors > Computing. link as of 2018-02-20:
http://catalog.etsu.edu/preview_program.php?catoid=21&poid=7950&returnto=1066

## What's a minor? ##

From the computing minor page:

> A minor is a secondary area of study outside of the major program of study with a structured curriculum composed of at least 18 semester credit hours of which at least 9 hours must be at the 3000-level or above.

## Gist of the catalog entry ##

### Course Requirements ###

- 11 c/h required courses:
    + CSCI 1250 - Intro. to CS I
    + CSCI 1260 - Intro to CS II
    + CSCI 1900 - Math for CS
- 12 c/h electives*:
    + 3 c/h: any CSCI course _excluding_ 1100, 1101, 1102, 1105, 1200
    + 9 c/h: 'upper division' (3xxx or 4xxx level) courses

\* exclude coops and internships

### Notes ###

- Students _must_ be advised
    + By who?
- Most CSCI courses have prerequisites that must be satisfied
    + Where to students seeking a minor go to find these?
- Must make C- or better in each course with the exception of CSCI 1250, where a B- or better is required
