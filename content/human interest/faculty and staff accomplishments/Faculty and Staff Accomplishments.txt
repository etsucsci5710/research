Faculty and Staff Accomplishments (Initial)

Dr.Tony Pittarese
	Grants
	2009–2015 “Support of ETSU’s Continuing Development of SAP-related Instruction,” Eastman
	Chemical, $84,000 to date (Pledge of ongoing financial support of $12,000 annually.)

	2009–2015 From a series of grant applications including “Using Cloud Computing to Address Lab
	Configuration Issues,” “Exploring Value Added Elements in e-Commerce Design through Student
	Projects,” and other research and teaching topics. Amazon.com Web Services, over $30,000 to date in
	usage credit for Amazon E2 Elastic Compute Cloud and Amazon Web Services.

	2010 “Business Process Management,” ETSU Presidential Grant-in-Aid, $1,500

	2009 “ETSU Application to Employ On-Site Hosting of SAP ERP Systems to Permit Custom
	Coursework and Related Curriculum Development,” SAP AG Germany, Value in the form of a
	campus license for local instances of SAP ERP, SAP NetWeaver, SAP CRM, and related resources in
	excess of $1,000,000.

	2009 “Initial Seed Funding in Support of Exploring Membership in the SAP University Alliances
	Program,” Eastman Chemical, $3,000

	2008 “Summer Training in Support of ETSU SAP Initiative,” ETSU Presidential Grant-in-Aid, $1,825.

	Awards
	Outstanding New Faculty Member, ETSU College of Business and Technology (2008 - 2009)
	Outstanding Teaching Award, ETSU College of Business and Techology (2010 - 2011)
	Outstanding Service Award, ETSU College of Business and Technology (2012 - 2013)
	Nominee, Outstanding Teaching Award, ETSU College of Business and Technology (2008 - 2009)
	Nominee, Outstanding Service Award, ETSU College of Business and Techology (2009 - 2010)
	Nominee, Outstanding Teaching Award, ETSU College of Business and Techology (2009 - 2010)
	Nominee, Outstanding Service Award, ETSU College of Business and Techology (2010 - 2011)


Dr. Asad Hoque
	Grants
	PI, East Tennessee State University RDC Major grant 2015 – 2016 - Development of an Integrated Simulator for Connected Vehicle Research. $10000
	PI, ETSU Research Foundation 2015 – 2016 - Establishment of Vehicular Networking Laboratory $10000
	PI, ETSU School of Graduate Studies 2016 – 2017 - Simulation and Validation of Data-Driven Driving Models for Large-Scale Urban Transportation Networks Using Big Data. $800

	Awards
	Faculty Excellence Award in Research: 2017 from the Dean of the College of Business and Technology of East Tennessee State University. 
	IEEE Outstanding Section Award 2016 as the Chair of IEEE Tri Cities section. 
	IEEE Outstanding Section Award 2017 as the Chair of IEEE Tri Cities section. 
