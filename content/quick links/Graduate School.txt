GRADUATE SCHOOL
Observation
i.	The users are categorized; therefore, the information of a page is dependent on the user group selected.
ii.	The information presented is up-to-date.
iii.	There is a staff directory which gives a brief description of each staff.
iv.	Most of the helpful and necessary links are connected to the graduate school.

Review
i.	The following links should be added to the content
	a.	link to international programs like International Buccaneer Buddy(IBB) and International Friendship Program (IFP)- https://www.etsu.edu/students/mcc/about.php . Having this link aids validates the claim of having an environment that supports and encourages diversity.
	b.	link to housing and meal plan - https://www.etsu.edu/students/housing/. This link helps a prospective student see the various accommodation and feeding option available to them
	c.	link to the library- https://libraries.etsu.edu/home.  This link takes a user to the library, it provides an opportunity for a prospective student to know the amount of resources available for use to support educational growth.
	d.	link to the International Office- https://www.etsu.edu/admissions/apply/international/ .
This link takes the user to the international office page which provides the necessary information needed to aid the application process  
Links
i.	https://www.etsu.edu/gradstud/default.php
ii.	https://www.etsu.edu/students/mcc/about.php
iii.	https://libraries.etsu.edu/home
iv.	https://www.etsu.edu/admissions/apply/international/
v.	https://www.etsu.edu/honors/international/internationalstudentscholarships/
