what all to link to?  including housing, off campus housing?
Undergraduate
-------------

-Advisement is required
-Links to
	- Computer Science Concentration Reqs and Tree Structure (as in above pdf link)
	- Information Systems Concentration Reqs and Tree Structure (as in above pdf link)
	- Information Techonology Concentration Reqs and Tree Structure (as in above pdf link)
Requirements: 
Complete CSCI 1100 or the UIT Proficiency Exam
- Complete 124 credit hours, including the ETSU General Education
Requirements, the common computing core, and the concentration-specific
courses.
- Earn a �B-�or better in CSCI 1250 and CSCI 1260. This requirement holds for
minors and majors alike.
- Earn a �C-�or better in all other CSCI major requirements other than CSCI 1250
and CSCI 1260.
- Complete all required courses in at most three attempts. An attempt is defined as
registering for and remaining enrolled in a course after the third week of the
semester. (Withdrawing from a course after the third week of the semester is
considered an attempt.) Computing majors and minors will be required to change
their program of study if these requirements cannot be met.
- Achieve a grade of �C� or better in ENGL 1010 and ENGL 1020
- Attain a GPA of 2.5 or better overall
- Attain a GPA of 2.5 or better in all computing courses
- Complete the California Critical Thinking Skills Test (CCTST) (Senior Exit Exam)
or other designated exit exam by the university. 
See pdf link for details - https://www.etsu.edu/cbat/computing/documents/advising_booklet_2017_2018.pdf


Graduate
--------
-Links to
	- Computer Science Concentration Reqs and Tree Structure
	- Information Techonology Concentration Reqs and Tree Structure
Requirements:
- (at least) ONE of the following:
	-undergraduate degree in CS with GPA >= 3.0 from a North American university
	-GRE with verbal >= 146, quantitative >= 146, and verbal + quantitative >= 292
	-professional record that demonstrates readiness for graduate study in computer science).
-Coursework in computer science. Students lacking this background may be required to take courses from the Foundation Courses listed below, with B- or better in each course.
-International students: TOEFL >= 213 (computerized) and GRE verbal >= 146.
-Three letters of recommendation and a statement of graduate study goals.

Applied Computer Science Foundation Courses Checklist
-----------------------------------------------------

Students must complete all foundation courses to be admitted unconditionally. For students admitted conditionally, all foundation courses must be completed within one year of the beginning of your first semester of study, with marks of B- or better (Note: see undergraduate catalog for course prerequisites):

CSCI 1250 Introduction to Computer Science I
CSCI 1260 Introduction to Computer Science II
CSCI 2020 Fundamentals of Database
CSCI 2150 Computer Organization
CSCI 2160 Assembly Language
CSCI 2210 Data Structures
CSCI 3230 Algorithms OR
an advanced systems course (compiler construction, programming languages, performance analysis, real time programming)
CSCI 4717 Computer Architecture OR CSCI 3400 Network Fundamentals
CSCI 4727 Operating Systems
Three from this list:
MATH 1110/1060 Calculus I
MATH 1120/1070 Calculus II
MATH 1530 Probability & Statistics
CSCI 1900 Math for Computer Science (i.e., discrete math)
MATH 4340 Graph theory

Information Technology Foundation Courses Checklist
----------------------------------------------------
Students must complete all foundation courses to be admitted unconditionally. For students admitted conditionally, all foundation courses must be completed within one year of the beginning of your first semester of study, with marks of B- or better (Note: see undergraduate catalog for course prerequisites):

CSCI 1250 Introduction to Computer Science I
CSCI 1260 Introduction to Computer Science II
CSCI 1710 World Wide Web Design
CSCI 2020 Fundamentals of Database
CSCI 2150 Computer Organization
CSCI 2910 Server-Side Web Programming
CSCI 3400 Network Fundamentals
CSCI 3250 Software Engineering I OR a junior/senior level programming or systems course
Two from this list:
MATH 1110 Calculus I
CSCI 1900 Math for Computer Science (i.e., discrete math)
MATH 1530 Probability & Statistics
MATH 2870 Probability & Statistics 2