ITS has a policy for Web Design and Publishing: https://www.etsu.edu/its/policies/web-traditional.php  

This page covers 
- Web Domain and Hosting (Content Management, Site Creation, and File Structure and Size), 
- Web Content (Course Materials on the Web, Size Limitations, Font, and Content Standards), - Footer Information (Links to External Sites, Accuracy and Relevance, Style and Grammar), 
- Color (Color Contrast), 
- Site Structure (Site Banners, Navigation Limitations, and Navigation Structure), 
- Commercial Transactions, 
    * in response to Dr. Pittarese’s mention of selling merchandise this section says:
      > “No ETSU web site (faculty server or main www.etsu.edu server) may collect credit card payments or other financial or personal sensitive information from users.”
- and Policy Application. 

This page should be studied and summarized as it has many things we have discussed and need further knowledge about.
