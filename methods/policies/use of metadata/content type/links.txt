=========================================================
Research Artifacts
--------------------------------------------------------
Best Image Format for Web
https://stackoverflow.com/questions/392635/website-image-formats-choosing-the-right-format-for-the-right-task

Content Consistency
https://omniupdate.com/blog/posts/2017/maintaining-a-consistent-voice-on-your-site.html

Accessibility Compliance
https://omniupdate.com/blog/posts/2018/avoid-accessibility-compliance-issues.html

Information About Accessibility
https://omniupdate.com/blog/posts/2016/accessibility-matters-is-your-website-up-to-code.html

Imagery Content
https://omniupdate.com/blog/posts/2018/ou-campus-image-editor.html

=========================================================
Supplemental Resources:
--------------------------------------------------------
Best Practices for Accessiblity
https://www.webaccessibility.com/best_practices.php

Institutional Website Accessibility Audit
https://omniupdate.com/campaigns/ouinsights-scan.html?marketing_detail=blog+Final+Check