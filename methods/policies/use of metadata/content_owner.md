# Content Ownership Metadata #

## Official university policies ##

I was unable to locate any official ETSU policy on the use of metadata to identify content ownership.

## General Practice ##

There seems to be some ambiguity between the use of "author" vs "content owner" for metadata?

### Guidelines ###

The article [Metadata fundamentals for intranets and websites](ttp://www.steptwo.com.au/papers/kmc_metadata/) indicates that 7 peices of metadata are common, namely:

- title
- keywords
- description
- publish date
- review date
- expiry date
- author

The article describes "author" as

> The original creator of the page, automatically captured by the publishing tool. This is used behind the scenes to route review and expiry messages, and also may be published on the website to allow feedback to be easily sent to the page owner.

...which does _not_ correspond one-to-one, in my mind, with the concept of "content owner".

A separate article on the same site, [Putting Metadata to Work](http://www.steptwo.com.au/papers/cmb_metadata/), is its own list of common metadata types, and includes:

- description
- content owner
- publishing dates
- review dates
- keywords

It describes _Content Owner_ as: 

> Lists the person (or role) responsible for keeping the content up-to-date. Use to:
>    - Build a system for automatically notifying the owner when the content needs to be reviewed or revised.
>    - Automatically route feedback messages to the relevant person. 

...which corresponds much more closely to my intuitive idea of the "content owner" role.

The articles both imply that most content management systems should support basic metadata tagging.

### The HTML `meta` tag ###

The HTML spec defines a document-level metadata element, `<meta>`. Most document metadata is defined using a `name` atrribute to specify the type of metadata, with a corresponding `content` attribute to specify the value for that type. There is a set of officially recognized values for the "name" attribute, as well as sets of officially recognized values for the "content" attribute corresponding to come values of the name attribute (e.g. when using `referrer`).

For specifying a document author in HTML, one would use:

```html
<meta name="author" content="Anonymous J. Author">
```

Documentation on MDN: https://developer.mozilla.org/en-US/docs/Web/HTML/Element/meta

Article with example usages: https://www.sitepoint.com/meta-tags-html-basics-best-practices/

## OU Campus ##

Page 48 of OIT's [OUCampus User Guide](https://www.etsu.edu/its/webtech/documents/ouc_userguide.pdf) shows the following fields available when creating a new page:

![OUCampus New Interior Page Form](./ouc_userguide_p48.png)

This implies that OUCampus supports Keyword, Description, and Author metadata when creating new site pages, but I was unable to find specific documentation on how OUCampus handles metadata. (Their official site is all sales with little technical info...)

